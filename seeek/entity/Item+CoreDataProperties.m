//
//  Item+CoreDataProperties.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "Item+CoreDataProperties.h"

@implementation Item (CoreDataProperties)

+ (NSFetchRequest<Item *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Item"];
}

@dynamic date;
@dynamic delete_flag;
@dynamic feed_id;
@dynamic image_url;
@dynamic link;
@dynamic text;
@dynamic title;
@dynamic view_count;
@dynamic category_id;

@end
