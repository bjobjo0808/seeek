//
//  Catogory+CoreDataProperties.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "Catogory+CoreDataProperties.h"

@implementation Catogory (CoreDataProperties)

+ (NSFetchRequest<Catogory *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Catogory"];
}

@dynamic catogory_id;
@dynamic date;
@dynamic name;
@dynamic sort;

@end
