//
//  Catogory+CoreDataProperties.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "Catogory+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Catogory (CoreDataProperties)

+ (NSFetchRequest<Catogory *> *)fetchRequest;

@property (nonatomic) int32_t catogory_id;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) int16_t sort;

@end

NS_ASSUME_NONNULL_END
