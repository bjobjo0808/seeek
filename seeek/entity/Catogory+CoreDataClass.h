//
//  Catogory+CoreDataClass.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Catogory : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Catogory+CoreDataProperties.h"
