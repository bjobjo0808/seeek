//
//  Feed+CoreDataProperties.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "Feed+CoreDataProperties.h"

@implementation Feed (CoreDataProperties)

+ (NSFetchRequest<Feed *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Feed"];
}

@dynamic catogory_id;
@dynamic date;
@dynamic feed_id;
@dynamic name;
@dynamic url;

@end
