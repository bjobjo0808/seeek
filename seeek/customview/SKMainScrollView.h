//
//  SKMainScrollView.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SKMainScrollView;

@protocol SKMainScrollViewDelegate <NSObject>

- (void)mainScrollView:(SKMainScrollView*)mainTableView scrollViewDidScroll:(CGPoint)offset;

@optional
- (void)mainScrollView:(SKMainScrollView*)mainTableView changePage:(int)page;

@end

@interface SKMainScrollView : UIScrollView <UIScrollViewDelegate>

- (void)setup;
- (void)setPage:(int)page;

@property(assign, nonatomic) id <SKMainScrollViewDelegate> mainScrollViewDelegate;

@end
