//
//  SKNotifyView.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/04.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKNotifyView.h"

@implementation SKNotifyView

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    self.layer.cornerRadius = self.cornerRadius;
    self.layer.borderColor = self.borderColor.CGColor;
    self.layer.borderWidth = self.borderWidth;
}


@end
