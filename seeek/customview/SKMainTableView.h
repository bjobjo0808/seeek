//
//  SKMainTableView.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/06.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKDataManager.h"
#import "SKMainCell.h"
#import "SKNoImageCell.h"
#import "SVModalWebViewController.h"

#import "Catogory+CoreDataClass.h"


@class SKMainTableView;

@protocol SKMainTableViewDelegate <NSObject>

- (void)mainTableView:(SKMainTableView*)mainTableView didSelectItem:(Item*)item;
- (void)mainTableView:(SKMainTableView*)mainTableView refresh:(BOOL)refresh;

@optional

@end

@interface SKMainTableView : UITableView <UITableViewDelegate, UITableViewDataSource>

- (void)setup;
- (void)renderListCatogory:(Catogory*)catogory;
- (void)renderList:(NSArray*)items;

@property(assign, nonatomic) id <SKMainTableViewDelegate> mainTableViewDelegate;

@end
