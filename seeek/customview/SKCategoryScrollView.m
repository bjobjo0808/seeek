//
//  SKCategoryScrollView.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKCategoryScrollView.h"

@implementation SKCategoryScrollView

- (void)setup{
    self.delegate = self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    NSLog(@"%f", scrollView.contentOffset.x);
}

@end
