//
//  SKMainTableView.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/06.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKMainTableView.h"

@implementation SKMainTableView{
    NSArray *_items;
    
    NSString *_mainCellIdentifier;
    NSString *_noImageCellIdentifier;
    
    NSIndexPath *_selectIndex;
    BOOL isShowPreView;
    
    SKDataManager *_dataManager;
}

- (void)setup{
    self.delegate = self;
    self.dataSource = self;
    
    _dataManager = [SKDataManager sharedManager];
}

- (void)renderListCatogory:(Catogory*)catogory {
    
    NSArray *items = [_dataManager getItemList:catogory feed:nil limit:15];
    [self renderList:items];
    
}

- (void)renderList:(NSArray*)items {
    
    _items = items;
    
    // ロケーションのセル
    _mainCellIdentifier = NSStringFromClass([SKMainCell class]);
    UINib *mainNib = [UINib nibWithNibName:_mainCellIdentifier bundle:nil];
    [self registerNib:mainNib forCellReuseIdentifier:_mainCellIdentifier];
    
    _noImageCellIdentifier = NSStringFromClass([SKNoImageCell class]);
    UINib *noImageNib = [UINib nibWithNibName:_noImageCellIdentifier bundle:nil];
    [self registerNib:noImageNib forCellReuseIdentifier:_noImageCellIdentifier];
    
    // テーブルレイアウト
    self.estimatedRowHeight = 120;
    self.rowHeight = UITableViewAutomaticDimension;
    
    // RefreshControl
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshControlChanged) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = self.refreshControl;
    self.alwaysBounceVertical = YES;
    
    [self reloadData];
}

#pragma mark - TableView Delegate, DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Item *item = [_items objectAtIndex:indexPath.row];
    
    if (item.image_url) {
        SKMainCell *cell = [tableView dequeueReusableCellWithIdentifier:_mainCellIdentifier forIndexPath:indexPath];
        [cell renderItem:item selected:(_selectIndex.row == indexPath.row)];
        return cell;
    } else {
        SKNoImageCell *cell = [tableView dequeueReusableCellWithIdentifier:_noImageCellIdentifier forIndexPath:indexPath];
        [cell renderItem:item selected:(_selectIndex.row == indexPath.row)];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Item *item = [_items objectAtIndex:indexPath.row];
    [self.mainTableViewDelegate mainTableView:self didSelectItem:item];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    NSLog(@"LOG");
}

- (void)refreshControlChanged {
    [self.refreshControl endRefreshing];
    [self.mainTableViewDelegate mainTableView:self refresh:YES];
//    [self setupFeedList];
}

@end
