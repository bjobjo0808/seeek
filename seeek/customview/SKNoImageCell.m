//
//  SKNoImageCell.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKNoImageCell.h"

@implementation SKNoImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.itemImageView.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.notifyView.hidden = YES;
    }
    
}

- (void)renderItem:(Item*)item selected:(BOOL)selected {
    
    self.item = item;
    
    self.titleLabel.text = self.item.title;
    self.feedNameLabel.text = [self.item.date SK_NSStringYYYYMMDDHHMM];
    
    if (item.view_count > 0) {
        self.notifyView.hidden = YES;
    } else {
        self.notifyView.hidden = NO;
    }
}

@end
