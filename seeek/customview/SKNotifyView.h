//
//  SKNotifyView.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/04.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface SKNotifyView : UIView

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;

@end
