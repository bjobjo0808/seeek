//
//  SKNoImageCell.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKNotifyView.h"

#import "Item+CoreDataClass.h"
#import "UIImageView+WebCache.h"

@interface SKNoImageCell : UITableViewCell

@property (nonatomic) Item *item;

@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedNameLabel;
@property (weak, nonatomic) IBOutlet SKNotifyView *notifyView;

- (void)renderItem:(Item*)item selected:(BOOL)selected;

@end
