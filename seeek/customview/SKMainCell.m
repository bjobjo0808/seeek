//
//  SKMainCell.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/03.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKMainCell.h"
#import "NSDate+SKAdditions.h"

@implementation SKMainCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.notifyView.hidden = YES;
    }
    
}

- (void)renderItem:(Item*)item selected:(BOOL)selected {
    
    self.item = item;
    
    self.itemImageView.hidden = NO;
    self.itemImageVIewHeight.constant = 160;
    [self.itemImageView sd_setImageWithURL:[NSURL URLWithString:item.image_url]];
    
    self.titleLabel.text = self.item.title;
    self.feedNameLabel.text = [self.item.date SK_NSStringYYYYMMDDHHMM];
 
    if (item.view_count > 0) {
        self.notifyView.hidden = YES;
    } else {
        self.notifyView.hidden = NO;
    }
}

@end
