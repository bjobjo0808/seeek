//
//  SKMainScrollView.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKMainScrollView.h"

@implementation SKMainScrollView

- (void)setup{
    self.delegate = self;
}

- (void)setPage:(int)page {
    self.contentOffset = CGPointMake(self.frame.size.width * page, 0);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
    
    [self.mainScrollViewDelegate mainScrollView:self scrollViewDidScroll:offset];
    
    if ( ((int)offset.x % (int)self.frame.size.width) == 0) {
        int page = offset.x / (int)self.frame.size.width;
        [self.mainScrollViewDelegate mainScrollView:self changePage:page];
    }
}

@end
