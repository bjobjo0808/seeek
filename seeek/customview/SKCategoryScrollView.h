//
//  SKCategoryScrollView.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SKCategoryScrollView : UIScrollView <UIScrollViewDelegate>

- (void)setup;

@end
