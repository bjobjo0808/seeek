//
//  UIView+SKAdditions.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (SKAdditions)

- (CGFloat)baseWidth;

- (void)showIndicator;
- (void)dismissIndicator;

@end
