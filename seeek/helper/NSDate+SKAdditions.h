//
//  NSDate+SKAdditions.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/04.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (SKAdditions)

- (NSString *)SK_NSStringYYYYMMDD;
- (NSString *)SK_NSStringYYYYMMDDHHMM;

@end
