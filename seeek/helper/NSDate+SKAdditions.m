//
//  NSDate+SKAdditions.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/04.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "NSDate+SKAdditions.h"

@implementation NSDate (SKAdditions)

- (NSString *)SK_NSStringYYYYMMDD {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    return [formatter stringFromDate:self];;
}

- (NSString *)SK_NSStringYYYYMMDDHHMM {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm"];
    return [formatter stringFromDate:self];;
}

@end
