//
//  UIView+SKAdditions.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "UIViewController+SKAdditions.h"

@implementation UIViewController (SKAdditions)

- (CGFloat)baseWidth {
    return self.view.frame.size.width;
}

- (void)showIndicator {
    [SVProgressHUD showWithStatus:@"LOADING"];
}

- (void)dismissIndicator {
    [SVProgressHUD dismiss];
}

@end
