//
//  SKDataManager.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>

#import "Item+CoreDataClass.h"
#import "Feed+CoreDataClass.h"
#import "Catogory+CoreDataClass.h"

@interface SKDataManager : NSObject

+ (SKDataManager *)sharedManager;

- (void) initializedFirstData;
- (NSArray*)getCategoryList:(int)limit;
- (NSArray*)getFeedList:(Catogory*)catogory limit:(int)limit;
- (NSArray*)getItemList:(Catogory*)catogory feed:(Feed*)feed limit:(int)limit;


- (BOOL)getInitializedFlag;
- (void)setInitializedFlag:(BOOL)flag;

@end
