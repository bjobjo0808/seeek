//
//  SKDataManager.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKDataManager.h"

@implementation SKDataManager {
    NSUserDefaults *_userDefaults;
}

static SKDataManager *sharedData_ = nil;

+ (SKDataManager*)sharedManager{
    @synchronized(self){
        if (!sharedData_) {
            sharedData_ = [SKDataManager new];
        }
    }
    return sharedData_;
}

- (id)init
{
    self = [super init];
    _userDefaults = [NSUserDefaults standardUserDefaults];
    return self;
}

#pragma mark - CoreData

- (void) initializedFirstData {
    Catogory *category1 = [Catogory MR_createEntity];
    category1.catogory_id = [self getCategoryIdWithIncrement];
    category1.name = @"NEWS";
    category1.date = [NSDate date];
    category1.sort = 1;
    
    Catogory *category2 = [Catogory MR_createEntity];
    category2.catogory_id = [self getCategoryIdWithIncrement];
    category2.name = @"HOBBY";
    category2.date = [NSDate date];
    category2.sort = 0;
    
    Catogory *category3 = [Catogory MR_createEntity];
    category3.catogory_id = [self getCategoryIdWithIncrement];
    category3.name = @"SPORTS";
    category3.date = [NSDate date];
    category3.sort = 2;
    
    Catogory *category4 = [Catogory MR_createEntity];
    category4.catogory_id = [self getCategoryIdWithIncrement];
    category4.name = @"地震";
    category4.date = [NSDate date];
    category4.sort = 3;
    
    Feed *feed1 = [Feed MR_createEntity];
    feed1.feed_id = [self getFeedIdWithIncrement];
    feed1.name = @"Yahooトピックス";
    feed1.url = @"http://news.yahoo.co.jp/pickup/rss.xml";
    feed1.date = [NSDate date];
    feed1.catogory_id = category1.catogory_id;
    
    Feed *feed2 = [Feed MR_createEntity];
    feed2.feed_id = [self getFeedIdWithIncrement];
    feed2.name = @"ライフハッカー";
    feed2.url = @"http://feeds.lifehacker.jp/rss/lifehacker/index.xml";
    feed2.date = [NSDate date];
    feed2.catogory_id = category2.catogory_id;
    
    Feed *feed3 = [Feed MR_createEntity];
    feed3.feed_id = [self getFeedIdWithIncrement];
    feed3.name = @"ニフティスポーツ";
    feed3.url = @"https://news.nifty.com/rss/topics_sports.xml";
    feed3.date = [NSDate date];
    feed3.catogory_id = category3.catogory_id;
    
    Feed *feed4 = [Feed MR_createEntity];
    feed4.feed_id = [self getFeedIdWithIncrement];
    feed4.name = @"地震速報 - livedoor 天気情報";
    feed4.url = @"http://weather.livedoor.com/forecast/rss/earthquake.xml";
    feed4.date = [NSDate date];
    feed4.catogory_id = category4.catogory_id;
    
    Feed *feed5 = [Feed MR_createEntity];
    feed5.feed_id = [self getFeedIdWithIncrement];
    feed5.name = @"GIGAZINE";
    feed5.url = @"http://gigazine.net/index.php?/news/rss_2.0/";
    feed5.date = [NSDate date];
    feed5.catogory_id = category2.catogory_id;
    
    
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (NSArray*)getCategoryList:(int)limit {
    NSFetchRequest *request = [Catogory MR_requestAll];
    [request setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"sort" ascending:YES]]];
    if (limit > 0) {
        [request setFetchLimit:limit];
    }
    return [Catogory MR_executeFetchRequest:request];
}

- (NSArray*)getFeedList:(Catogory*)catogory limit:(int)limit {
    
    // 検索条件
    NSMutableArray *whereArray = [NSMutableArray new];
    if (catogory)
    {
        [whereArray addObject:[NSPredicate predicateWithFormat:@"catogory_id = %d", catogory.catogory_id]];
    }
    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:whereArray];
    
    // 検索実行
    NSFetchRequest *request = [Feed MR_requestAllWithPredicate:predicate];
    [request setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"feed_id" ascending:YES]]];
    if (limit > 0) {
        [request setFetchLimit:limit];
    }
    return [Feed MR_executeFetchRequest:request];
}

- (NSArray*)getItemList:(Catogory*)catogory feed:(Feed*)feed limit:(int)limit {
    // 検索条件
    NSMutableArray *whereArray = [NSMutableArray new];
    if (catogory)
    {
        [whereArray addObject:[NSPredicate predicateWithFormat:@"category_id = %d", catogory.catogory_id]];
    }
    if (feed)
    {
        [whereArray addObject:[NSPredicate predicateWithFormat:@"feed_id = %d", feed.feed_id]];
    }
    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:whereArray];
    
    // 検索実行
    NSFetchRequest *request = [Item MR_requestAllWithPredicate:predicate];
    [request setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO]]];
    if (limit > 0) {
        [request setFetchLimit:limit];
    }
    return [Item MR_executeFetchRequest:request];
}

#pragma mark - NSUserDefaults

- (int)getCategoryIdWithIncrement {
    int categoryId = (int)[_userDefaults integerForKey:@"CategoryId"];
    [_userDefaults setInteger:(categoryId + 1) forKey:@"CategoryId"];
    return categoryId;
    
}

- (int)getFeedIdWithIncrement {
    int categoryId = (int)[_userDefaults integerForKey:@"FeedId"];
    [_userDefaults setInteger:(categoryId + 1) forKey:@"FeedId"];
    return categoryId;
}

- (int)getItemIdWithIncrement {
    int categoryId = (int)[_userDefaults integerForKey:@"ItemId"];
    [_userDefaults setInteger:(categoryId + 1) forKey:@"ItemId"];
    return categoryId;
}

- (BOOL)getInitializedFlag {
    return [_userDefaults boolForKey:@"InitializedFlag"];
}

- (void)setInitializedFlag:(BOOL)flag {
    [_userDefaults setBool:flag forKey:@"InitializedFlag"];
}

@end
