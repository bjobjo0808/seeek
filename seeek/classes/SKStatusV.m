//
//  SKStatusV.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/03.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKStatusV.h"

@implementation SKStatusV


- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor SK_darkSkyBlueColor];
}

@end
