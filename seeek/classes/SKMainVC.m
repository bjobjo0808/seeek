//
//  SKMainVC.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/02.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKMainVC.h"
#import "SKMainCell.h"

@interface SKMainVC () {
    
    int _index;
    
    SKRssManager *_rssManager;
    SKDataManager *_dataManager;
    
    NSArray *_items;
    NSArray *_categoryList;
    
    NSString *_mainCellIdentifier;
    
    NSIndexPath *_selectIndex;
    BOOL isShowPreView;
    
    BOOL _isShowCategoryLabel;
    int _showCategoryLabelTimer;
}

@end

@implementation SKMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _index = 0;
    
    _rssManager = [SKRssManager sharedManager];
    _rssManager.rssManagerDelegate = self;
    _dataManager = [SKDataManager sharedManager];
    
    if (![_dataManager getInitializedFlag]) {
        // 初期データ挿入
        [_dataManager initializedFirstData];
        [_dataManager setInitializedFlag:YES];
    }
    
    _categoryList = [_dataManager getCategoryList:0];
    
    self.scrollView.mainScrollViewDelegate = self;
    self.leftTableVIew.mainTableViewDelegate = self;
    self.tableView.mainTableViewDelegate = self;
    self.rightTableView.mainTableViewDelegate = self;
    
    [self.scrollView setup];
    [self.categoryScrollView setup];
    [self.leftTableVIew setup];
    [self.tableView setup];
    [self.rightTableView setup];
    
    // 初期ページをセット
    [self.scrollView setPage:1];
    
    [self setupFeedList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupFeedList
{
    // left
    Catogory *leftCatogory = _index == 0 ? [_categoryList lastObject] : [_categoryList objectAtIndex:(_index - 1)] ;
    [self.leftTableVIew renderListCatogory:leftCatogory];
    
    // center
    Catogory *centerCatogory = [_categoryList objectAtIndex:_index];
    [self.tableView renderListCatogory:centerCatogory];
    
    // right
    Catogory *rightCatogory = _index >= [_categoryList count] - 1 ? [_categoryList firstObject] : [_categoryList objectAtIndex:(_index + 1)] ;
    [self.rightTableView renderListCatogory:rightCatogory];
    
    // label
    self.leftCategoryLabel.text = leftCatogory.name;
    self.centerCategoryLabel.text = centerCatogory.name;
    self.rightCategoryLabel.text = rightCatogory.name;
}

//- (void)showCategoryLabel {
//    _showCategoryLabelTimer = 0;
//    if (_isShowCategoryLabel ) {
//        return;
//    }
//    
//    _isShowCategoryLabel = YES;
//    self.categoryScrollViewHeight.constant = 20;
//    [self.view layoutIfNeeded];
//
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        while (_showCategoryLabelTimer < 2) {
//            [NSThread sleepForTimeInterval:0.5];
//            _showCategoryLabelTimer++;
//        }
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//                [UIView animateWithDuration:UINavigationControllerHideShowBarDuration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
//                    
//                    self.categoryScrollViewHeight.constant = 0;
//                    [self.view layoutIfNeeded];
//                    
//                
//            }                completion:^(BOOL finished){
//                _isShowCategoryLabel = NO;
//            }];
//        });
//    });
//}

#pragma mark - SKMainTableViewDelegate

- (void)mainTableView:(SKMainTableView*)mainTableView didSelectItem:(Item*)item {
    
    // カウントアップ
    item.view_count++;
    NSLog(@"AAA:%d", item.view_count);
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    // WebViewを開く
    SKWebViewController *viewController = [SKWebViewController getViewControllerWithUrl:item.link];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)mainTableView:(SKMainTableView*)mainTableView refresh:(BOOL)refresh {
    [_rssManager loadAllRss];
    [self setupFeedList];
}

#pragma mark - SKRssManagerDelegate

- (void)rssManager:(SKRssManager*)rssManager startLoading:(BOOL)ret {
    NSLog(@"START");
    [UIView animateWithDuration:UINavigationControllerHideShowBarDuration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.loadingViewHeight.constant = 20;
        [self.view layoutIfNeeded];
    }                completion:^(BOOL finished){
    }];
}
- (void)rssManager:(SKRssManager*)rssManager endLoading:(BOOL)ret {
    [UIView animateWithDuration:UINavigationControllerHideShowBarDuration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.loadingViewHeight.constant = 0;
        [self.view layoutIfNeeded];
    }                completion:^(BOOL finished){
    }];
}

#pragma mark - SKMainScrollViewDelegate

- (void)mainScrollView:(SKMainScrollView*)mainTableView scrollViewDidScroll:(CGPoint)offset {
    self.categoryScrollView.contentOffset = offset;
    
    // 375
    float alpha = fabs(offset.x - self.baseWidth) / self.baseWidth * 3;
    self.leftCategoryLabel.alpha = alpha;
    self.centerCategoryLabel.alpha = 1 - alpha;
    self.rightCategoryLabel.alpha = alpha;
}

- (void)mainScrollView:(SKMainScrollView*)mainTableView changePage:(int)page {
    if (page == 0 || page == 2) {
        if (page == 0) {
            _index--;
            if (_index < 0) {
                _index = (int)[_categoryList count] - 1;
            }
        } else {
            _index++;
            if (_index >= [_categoryList count]) {
                _index = 0;
            }
        }
        [self.scrollView setPage:1];
        
        [self setupFeedList];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.leftTableVIew scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        [self.rightTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

#pragma mark - SKMainTableViewDelegate

@end
