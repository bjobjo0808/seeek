//
//  SKWebViewController.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SKWebViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic) NSString *url;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;


+ (instancetype)getViewControllerWithUrl:(NSString*)url;

@end
