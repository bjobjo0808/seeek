//
//  SKWebViewController.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/07.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKWebViewController.h"

@interface SKWebViewController () {
    int _loadingCount;
    BOOL _isFirstLoading;
}

@end

@implementation SKWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *firstURL = [NSURL URLWithString:self.url];
    _isFirstLoading = YES;
    [self.webView loadRequest:[NSURLRequest requestWithURL:firstURL]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self dismissIndicator];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)actionBackButton:(id)sender {
    if (self.webView.canGoBack) {
        [self.webView goBack];
    }
}
- (IBAction)actionNextButton:(id)sender {
    if (self.webView.canGoForward) {
        [self.webView goForward];
    }
}
- (IBAction)actionShareButton:(id)sender {
}

- (IBAction)actionCloseButton:(UIButton *)sender {
    // navigationController管理するコントローラーの数によって処理を分ける
    if ([[self.navigationController viewControllers] count] == 1) {
        // モーダル閉じる
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        //　プッシュ閉じる
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)renderButton {
//    self.backButton.alpha = self.webView.canGoBack ? 100 : 50;
    
    [self.backButton setImage:self.webView.canGoBack ? [UIImage imageNamed:@"back_button"] : [UIImage imageNamed:@"back_button_disable"] forState:UIControlStateNormal];
    [self.nextButton setImage:self.webView.canGoForward ? [UIImage imageNamed:@"next_button"] : [UIImage imageNamed:@"next_button_disable"] forState:UIControlStateNormal];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView*)webView {
    if (_isFirstLoading) {
        [self showIndicator];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    _loadingCount++;
    
}

- (void)webViewDidFinishLoad:(UIWebView*)webView {
    _loadingCount--;
    if (_isFirstLoading) {
        [self dismissIndicator];
        _isFirstLoading = NO;
    }
    if (_loadingCount == 0) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
    
    [self renderButton];
}

- (void)webView:(UIWebView*)webView
didFailLoadWithError:(NSError*)error {
    _loadingCount--;
    if (_isFirstLoading) {
        [self dismissIndicator];
        _isFirstLoading = NO;
    }
    if (_loadingCount == 0) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
    
    [self renderButton];
}

#pragma mark - Get ViewController

+ (instancetype)getViewControllerWithUrl:(NSString*)url {
    SKWebViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    viewController.url = url;
    return viewController;
}

@end
