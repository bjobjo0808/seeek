//
//  SKRssManager.m
//  seeek
//
//  Created by Shun Takagi on 2017/02/03.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import "SKRssManager.h"

@implementation SKRssManager {
    
    dispatch_queue_t _queue;
    int _queueCount;
    
    AFHTTPSessionManager *_manager;
    SKDataManager *_dataManager;
    
    NSMutableArray *_rssFeedList;
    
    NSDateFormatter* _dcDateFormatter;
    NSDateFormatter* _pubDateFormatter;
    
    void (^_success)(BOOL isUpdate);
    void (^_failure)(NSError *error);
    
    int feedIndex;
    BOOL _isUpdate;
    BOOL _isLoading;
}

static SKRssManager *sharedData_ = nil;

+ (SKRssManager*)sharedManager{
    @synchronized(self){
        if (!sharedData_) {
            sharedData_ = [SKRssManager new];
        }
    }
    return sharedData_;
}

- (id)init
{
    self = [super init];
    
    _queue = dispatch_queue_create("queue", DISPATCH_QUEUE_CONCURRENT);
    
    _dataManager = [SKDataManager sharedManager];
    
    _manager = [AFHTTPSessionManager manager];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    _dcDateFormatter = [[NSDateFormatter alloc] init];
    [_dcDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    [_dcDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    _pubDateFormatter = [[NSDateFormatter alloc] init];
    [_pubDateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss Z"];
    [_pubDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    return self;
}

- (void)loadAllRss {
        NSArray *categoryList = [_dataManager getCategoryList:0];
        for (Catogory *category in categoryList) {
            NSArray *feedList = [_dataManager getFeedList:category limit:0];
            _queueCount += [feedList count];
            for (Feed *feed in feedList) {
                dispatch_barrier_async(_queue, ^{
                    if (_queueCount == 0) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.rssManagerDelegate rssManager:self startLoading:YES];
                        });
                    }
                    [self getRss:feed];
                });
            }
        }
}

- (void)getRss:(Feed*)feed {
    
    NSLog(@"BBB START:%d", _queueCount);
    DDLogInfo(@"GET RSS : %@", feed.name);
    
    __block BOOL isLoadRss = YES;
    __weak __typeof(self)weakSelf = self;
    [_manager GET:feed.url
       parameters:nil
          success:^(NSURLSessionDataTask *operation, id responseObject) {
              
              NSDictionary *resultDict = [XMLReader dictionaryForXMLData:(NSData*)responseObject error:nil];
              
              if (resultDict[@"rdf:RDF"]) {
                  NSLog(@"RDF");
                  // RSSデータ
                  NSDictionary *channel = resultDict[@"rdf:RDF"][@"channel"];
                  NSLog(@"RDF NAME    : %@", channel[@"title"][@"text"]);
                  
                  // アイテム 登録
                  for (NSDictionary *dic in resultDict[@"rdf:RDF"][@"item"]) {
                      
//                      // データあるか
//                      Item *item = [Item MR_findFirstByAttribute:@"text" withValue:dic[@"title"][@"text"]];
//                      if (!item) {
//                          item = [Item MR_createEntity];
//                      } else {
//                          continue;
//                      }
                      
                      _isUpdate = YES;
                      
//                      item.site_name = channel[@"title"][@"text"];
//                      item.text      = dic[@"title"][@"text"];
//                      item.link_url  = dic[@"link"][@"text"];
//                      item.feed_id   = feed.feed_id;
//                      item.mylist_id = feed.mylist_id;
                      
                      // 時間
                      NSDate *date = [NSDate date];
                      if (dic[@"dc:date"]) {
                          date = [_dcDateFormatter dateFromString:dic[@"dc:date"][@"text"]];
                      } else if (dic[@"pubDate"]) {
                          date = [_pubDateFormatter dateFromString:dic[@"pubDate"][@"text"]];
                      }
//                      item.date = date;
                      
                      // 画像
                      NSString *desc = dic[@"content:encoded"][@"text"];
                      
                      NSArray *array = [desc componentsSeparatedByString:@"<img"];
                      if ([array count] > 1) {
                          array = [array[1] componentsSeparatedByString:@"src=\""];
                          if ([array count] > 1) {
                              desc = (NSString*)array[1];
                              NSString *imgUrl = [desc componentsSeparatedByString:@"\""][0];
                              NSRange searchResult = [imgUrl rangeOfString:@".jpg"];
                              if(searchResult.location != NSNotFound){
//                                  item.image_url = imgUrl;
                              }
                              searchResult = [imgUrl rangeOfString:@".png"];
                              if(searchResult.location != NSNotFound){
//                                  item.image_url = imgUrl;
                              }
                          }
                      }
                      
//                      NSLog(@"ADD ITEM : %@", item.text);
                      
//                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                  }
                  
              } else if ([resultDict[@"rss"][@"version"] isEqualToString:@"2.0"]) {
                  
                  // RSS2.0データ
                  NSDictionary *channel = resultDict[@"rss"][@"channel"];
                  NSLog(@"REQEST RSS NAME    : %@", channel[@"title"][@"text"]);
                  
                  // アイテム 登録
                  for (NSDictionary *dic in channel[@"item"]) {
                      
                      // データあるか
                      Item *item = [Item MR_findFirstByAttribute:@"title" withValue:dic[@"title"][@"text"]];
                      if (!item) {
                          item = [Item MR_createEntity];
                      } else {
                          DDLogInfo(@"SKIP ITEM");
                          DDLogInfo(@"ITEM TITLE   : %@", item.title);
                          continue;
                      }
                      
                      _isUpdate = YES;
                      
                      item.title = dic[@"title"][@"text"];
                      if (dic[@"description"]) {
                          item.text = dic[@"description"][@"text"];
                      }
                      item.link = dic[@"link"][@"text"];
                      item.delete_flag = false;
                      item.view_count = 0;
                      
                      // 時間
                      NSDate *date = [NSDate date];
                      if (dic[@"dc:date"]) {
                          date = [_dcDateFormatter dateFromString:dic[@"dc:date"][@"text"]];
                      } else if (dic[@"pubDate"]) {
                          date = [_pubDateFormatter dateFromString:dic[@"pubDate"][@"text"]];
                      }
                      item.date = date;
                      
                      
                      // 画像
                      NSString *desc = dic[@"description"][@"text"];
                      // 本文解析
                      NSArray *array = [desc componentsSeparatedByString:@"<img"];
                      if ([array count] > 1) {
                          array = [array[1] componentsSeparatedByString:@"src=\""];
                          if ([array count] > 1) {
                              desc = (NSString*)array[1];
                              NSString *imgUrl = [desc componentsSeparatedByString:@"\""][0];
                              NSRange searchResult = [imgUrl rangeOfString:@".jpg"];
                              if(searchResult.location != NSNotFound){
                                  item.image_url = imgUrl;
                              }
                              searchResult = [imgUrl rangeOfString:@".png"];
                              if(searchResult.location != NSNotFound){
                                  item.image_url = imgUrl;
                              }
                          }
                      }
                      // 本文解析
                      if (!item.image_url) {
                          NSString *desc = dic[@"content:encoded"][@"text"];
                          NSArray *array = [desc componentsSeparatedByString:@"<img"];
                          if ([array count] > 1) {
                              array = [array[1] componentsSeparatedByString:@"src=\""];
                              if ([array count] > 1) {
                                  desc = (NSString*)array[1];
                                  NSString *imgUrl = [desc componentsSeparatedByString:@"\""][0];
                                  NSRange searchResult = [imgUrl rangeOfString:@".jpg"];
                                  if(searchResult.location != NSNotFound){
                                      item.image_url = imgUrl;
                                  }
                                  searchResult = [imgUrl rangeOfString:@".png"];
                                  if(searchResult.location != NSNotFound){
                                      item.image_url = imgUrl;
                                  }
                              }
                          }
                      }
                      
//                      if (!item.image_url) {
//                          continue;
//                      }
                      
                      item.feed_id = feed.feed_id;
                      item.category_id = feed.catogory_id;
                      
                      
                      DDLogInfo(@"ADD ITEM");
                      DDLogInfo(@"ITEM TITLE   : %@", item.title);
                      DDLogInfo(@"ITEM TEXT    : %@", item.text);
                      DDLogInfo(@"ITEM IMAGE   : %@", item.image_url);
                      DDLogInfo(@"ITEM LINK    : %@", item.link);
                      DDLogInfo(@"ITEM DATE    : %@", item.date);
                      DDLogInfo(@"FEED ID      : %d", item.feed_id);
                      DDLogInfo(@"CATEGORY ID  : %d", item.category_id);
                      
                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                  }
              }
              
//              // オーバー分削除
//              NSPredicate *peopleFilter = [NSPredicate predicateWithFormat:@"feed_id = %@", feed.feed_id];
//              NSFetchRequest *request = [Item MR_requestAllWithPredicate:peopleFilter];
//              [request setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES]]];
//              NSMutableArray *itemList = [[Item MR_executeFetchRequest:request] mutableCopy];
//              BOOL isUpdate = NO;
//              while ([itemList count] > [Utils saveLimit]) {
//                  Item *item = itemList[0];
//                  [item MR_deleteEntity];
//                  isUpdate = YES;
//                  [itemList removeObjectAtIndex:0];
//              }
//              if (isUpdate) {
//                  [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
//              }
              
//              [weakSelf loadRSSData];
              
              NSLog(@"BBB END:%d", _queueCount);
              _queueCount--;
              isLoadRss = NO;
          }
          failure:^(NSURLSessionDataTask *operation, NSError *error) {
              DDLogError(@"Error: %@", error);
              _queueCount--;
              isLoadRss = NO;
          }];
        
    while (isLoadRss) {
        [NSThread sleepForTimeInterval:0.1f];
    }
    
    if (_queueCount == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.rssManagerDelegate rssManager:self endLoading:YES];
        });
    }
}

//
///**
// *  RSS URLからフィード名を返す
// *
// */
//- (void)setFeedMetabyReqest:(NSString*)url
//                    success:(void (^)(NSString *name))success
//                    failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure
//{
//    [_manager GET:url
//       parameters:nil
//          success:^(NSURLSessionDataTask *operation, id responseObject) {
//              NSDictionary *resultDict = [XMLReader dictionaryForXMLData:(NSData*)responseObject error:nil];
//              //NSLog(@"Success: %@", [resultDict description]);
//              
//              // バージョン
//              if (resultDict[@"rdf:RDF"]) {
//                  NSLog(@"RDF");
//                  // RSSデータ
//                  NSDictionary *channel = resultDict[@"rdf:RDF"][@"channel"];
//                  NSLog(@"RSS NAME    : %@", channel[@"title"][@"text"]);
//                  success(channel[@"title"][@"text"]);
//                  
//              } else if ([resultDict[@"rss"][@"version"] isEqualToString:@"2.0"]) {
//                  NSLog(@"RSS VERSION : 2.0");
//                  // RSSデータ
//                  NSDictionary *channel = resultDict[@"rss"][@"channel"];
//                  NSLog(@"RSS NAME    : %@", channel[@"title"][@"text"]);
//                  success(channel[@"title"][@"text"]);
//              }
//              
//          }
//          failure:failure];
//}
//
///**
// *  フィードリストのフィードを更新する
// */
//- (void)loadRSSByFeedList:(NSMutableArray*)feedList
//                  success:(void (^)(BOOL isUpdate))success
//                  failure:(void (^)(NSError *error))failure
//{
//    NSLog(@"LOAD RSS FEED LIST");
//    
//    if (_isLoading) {
//        failure(nil);
//        return;
//    }
//    
//    _success = success;
//    _failure = failure;
//    
//    _rssFeedList = feedList;
//    
//    feedIndex = 0;
//    _isUpdate = NO;
//    
//    [self loadRSSData];
//}
//
//- (void)loadRSSData{
//    
//    _isLoading = YES;
//    
//    if ([_rssFeedList count] > feedIndex) {
//        [self reqest:_rssFeedList[feedIndex]];
//        feedIndex++;
//    } else {
//        _isLoading = NO;
//        _success(_isUpdate);
//    }
//}
//
//- (void) autoLoad:(void (^)(BOOL isUpdate))success
//          failure:(void (^)(NSError *error))failure
//{
//    NSLog(@"AUTO LOAD");
//    if (_isLoading) {
//        return;
//    }
//    
//    _success = success;
//    _failure = failure;
//    
//    //    NSFetchRequest *request = [Feed MR_createFetchRequest];
//    //    [request setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"last_update_date" ascending:YES]]];
//    //    [request setFetchLimit:5];
//    //    _rssFeedList = [[Feed MR_executeFetchRequest:request] mutableCopy];
//    //
//    //    for (Feed *feed in _rssFeedList) {
//    //        NSLog(@"%@", feed.last_update_date);
//    //    }
//    
//    feedIndex = 0;
//    _isUpdate = NO;
//    
//    [self loadRSSData];
//}
//
//
//- (void)reqest:(Feed*)feed{
//    
//    [Utils sendGAEventWithCategory:SYSTEM action:UPDATE_FEED label:feed.name value:nil optional:nil];
//    
//    feed.last_update_date = [NSDate date];
//    
//    // カウントアップ
//    NSArray *itemList = [Item MR_findByAttribute:@"feed_id" withValue:feed.feed_id];
//    for (Item *item in itemList) {
//        if ([item.view_count intValue] == 0) {
//            int count = [item.view_count intValue];
//            item.view_count = @(count + 1);
//        }
//    }
//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
//    
//    __weak __typeof(self)weakSelf = self;
//    [_manager GET:feed.url
//       parameters:nil
//          success:^(NSURLSessionDataTask *operation, id responseObject) {
//              
//              NSDictionary *resultDict = [XMLReader dictionaryForXMLData:(NSData*)responseObject error:nil];
//              //NSLog(@"Success: %@", [resultDict description]);
//              
//              if (resultDict[@"rdf:RDF"]) {
//                  NSLog(@"RDF");
//                  // RSSデータ
//                  NSDictionary *channel = resultDict[@"rdf:RDF"][@"channel"];
//                  NSLog(@"RDF NAME    : %@", channel[@"title"][@"text"]);
//                  
//                  // アイテム 登録
//                  for (NSDictionary *dic in resultDict[@"rdf:RDF"][@"item"]) {
//                      
//                      // データあるか
//                      Item *item = [Item MR_findFirstByAttribute:@"text" withValue:dic[@"title"][@"text"]];
//                      if (!item) {
//                          item = [Item MR_createEntity];
//                      } else {
//                          continue;
//                      }
//                      
//                      _isUpdate = YES;
//                      
//                      item.site_name = channel[@"title"][@"text"];
//                      item.text      = dic[@"title"][@"text"];
//                      item.link_url  = dic[@"link"][@"text"];
//                      item.feed_id   = feed.feed_id;
//                      item.mylist_id = feed.mylist_id;
//                      
//                      // 時間
//                      NSDate *date = [NSDate date];
//                      if (dic[@"dc:date"]) {
//                          date = [_dcDateFormatter dateFromString:dic[@"dc:date"][@"text"]];
//                      } else if (dic[@"pubDate"]) {
//                          date = [_pubDateFormatter dateFromString:dic[@"pubDate"][@"text"]];
//                      }
//                      item.date = date;
//                      
//                      // 画像
//                      NSString *desc = dic[@"content:encoded"][@"text"];
//                      
//                      NSArray *array = [desc componentsSeparatedByString:@"<img"];
//                      if ([array count] > 1) {
//                          array = [array[1] componentsSeparatedByString:@"src=\""];
//                          if ([array count] > 1) {
//                              desc = (NSString*)array[1];
//                              NSString *imgUrl = [desc componentsSeparatedByString:@"\""][0];
//                              NSRange searchResult = [imgUrl rangeOfString:@".jpg"];
//                              if(searchResult.location != NSNotFound){
//                                  item.image_url = imgUrl;
//                              }
//                              searchResult = [imgUrl rangeOfString:@".png"];
//                              if(searchResult.location != NSNotFound){
//                                  item.image_url = imgUrl;
//                              }
//                          }
//                      }
//                      
//                      NSLog(@"ADD ITEM : %@", item.text);
//                      
//                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
//                  }
//                  
//              } else if ([resultDict[@"rss"][@"version"] isEqualToString:@"2.0"]) {
//                  // RSSデータ
//                  NSDictionary *channel = resultDict[@"rss"][@"channel"];
//                  NSLog(@"REQEST RSS NAME    : %@", channel[@"title"][@"text"]);
//                  // アイテム 登録
//                  for (NSDictionary *dic in channel[@"item"]) {
//                      
//                      // データあるか
//                      Item *item = [Item MR_findFirstByAttribute:@"text" withValue:dic[@"title"][@"text"]];
//                      if (!item) {
//                          item = [Item MR_createEntity];
//                      } else {
//                          continue;
//                      }
//                      
//                      _isUpdate = YES;
//                      
//                      item.site_name = channel[@"title"][@"text"];
//                      item.text = dic[@"title"][@"text"];
//                      item.link_url = dic[@"link"][@"text"];
//                      item.feed_id = feed.feed_id;
//                      item.mylist_id = feed.mylist_id;
//                      
//                      // 時間
//                      NSDate *date = [NSDate date];
//                      if (dic[@"dc:date"]) {
//                          date = [_dcDateFormatter dateFromString:dic[@"dc:date"][@"text"]];
//                      } else if (dic[@"pubDate"]) {
//                          date = [_pubDateFormatter dateFromString:dic[@"pubDate"][@"text"]];
//                      }
//                      item.date = date;
//                      
//                      // 画像
//                      NSString *desc = dic[@"description"][@"text"];
//                      NSArray *array = [desc componentsSeparatedByString:@"<img"];
//                      if ([array count] > 1) {
//                          array = [array[1] componentsSeparatedByString:@"src=\""];
//                          if ([array count] > 1) {
//                              desc = (NSString*)array[1];
//                              NSString *imgUrl = [desc componentsSeparatedByString:@"\""][0];
//                              NSRange searchResult = [imgUrl rangeOfString:@".jpg"];
//                              if(searchResult.location != NSNotFound){
//                                  item.image_url = imgUrl;
//                              }
//                              searchResult = [imgUrl rangeOfString:@".png"];
//                              if(searchResult.location != NSNotFound){
//                                  item.image_url = imgUrl;
//                              }
//                          }
//                      }
//                      if (!item.image_url) {
//                          NSString *desc = dic[@"content:encoded"][@"text"];
//                          NSArray *array = [desc componentsSeparatedByString:@"<img"];
//                          if ([array count] > 1) {
//                              array = [array[1] componentsSeparatedByString:@"src=\""];
//                              if ([array count] > 1) {
//                                  desc = (NSString*)array[1];
//                                  NSString *imgUrl = [desc componentsSeparatedByString:@"\""][0];
//                                  NSRange searchResult = [imgUrl rangeOfString:@".jpg"];
//                                  if(searchResult.location != NSNotFound){
//                                      item.image_url = imgUrl;
//                                  }
//                                  searchResult = [imgUrl rangeOfString:@".png"];
//                                  if(searchResult.location != NSNotFound){
//                                      item.image_url = imgUrl;
//                                  }
//                              }
//                          }
//                      }
//                      
//                      NSLog(@"ADD ITEM : %@", item.text);
//                      
//                      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
//                  }
//              }
//              
//              // オーバー分削除
//              NSPredicate *peopleFilter = [NSPredicate predicateWithFormat:@"feed_id = %@", feed.feed_id];
//              NSFetchRequest *request = [Item MR_requestAllWithPredicate:peopleFilter];
//              [request setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES]]];
//              NSMutableArray *itemList = [[Item MR_executeFetchRequest:request] mutableCopy];
//              BOOL isUpdate = NO;
//              while ([itemList count] > [Utils saveLimit]) {
//                  Item *item = itemList[0];
//                  [item MR_deleteEntity];
//                  isUpdate = YES;
//                  [itemList removeObjectAtIndex:0];
//              }
//              if (isUpdate) {
//                  [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
//              }
//              
//              [weakSelf loadRSSData];
//          }
//          failure:^(NSURLSessionDataTask *operation, NSError *error) {
//              NSLog(@"Error: %@", error);
//              [weakSelf loadRSSData];
//          }];
//}
@end
