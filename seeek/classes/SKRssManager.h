//
//  SKRssManager.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/03.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AFNetworking.h>
#import <XMLReader.h>
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>

#import "Item+CoreDataClass.h"
#import "Feed+CoreDataClass.h"
#import "Catogory+CoreDataClass.h"

#import "SKDataManager.h"

//#import "Item+CoreDataClass.h"

@class SKRssManager;

@protocol SKRssManagerDelegate <NSObject>
@optional
- (void)rssManager:(SKRssManager*)rssManager startLoading:(BOOL)ret;
- (void)rssManager:(SKRssManager*)rssManager endLoading:(BOOL)ret;
@end

@interface SKRssManager : NSObject

//@property (nonatomic, strong) SKRssManager* rssManager;
+ (SKRssManager *)sharedManager;

- (void)loadAllRss;
- (void)getRss:(Feed*)feed;

//- (void)loadRSSByFeedList:(NSMutableArray*)feedList
//                  success:(void (^)(BOOL isUpdate))success
//                  failure:(void (^)(NSError *error))failure;
//
//- (void)loadRSSData;
//
//- (void)setFeedMetabyReqest:(NSString*)url
//                    success:(void (^)(NSString *name))success
//                    failure:(void (^)(NSURLSessionDataTask *operation, NSError *error))failure;
//
//- (void) autoLoad:(void (^)(BOOL isUpdate))success
//          failure:(void (^)(NSError *error))failure;

@property(assign, nonatomic) id <SKRssManagerDelegate> rssManagerDelegate;

@end
