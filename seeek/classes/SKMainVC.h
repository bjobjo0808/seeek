//
//  SKMainVC.h
//  seeek
//
//  Created by Shun Takagi on 2017/02/02.
//  Copyright © 2017年 Shun Takagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVWebViewController/SVWebViewController.h>
#import "SKRssManager.h"
#import "SKDataManager.h"

#import "SKMainTableView.h"
#import "SKMainScrollView.h"
#import "SKCategoryScrollView.h"
#import "SKWebViewController.h"

#import "Item+CoreDataClass.h"
#import "Feed+CoreDataClass.h"
#import "Catogory+CoreDataClass.h"

@interface SKMainVC : UIViewController <SKMainTableViewDelegate, SKMainScrollViewDelegate, SKRssManagerDelegate>

@property (weak, nonatomic) IBOutlet SKMainScrollView *scrollView;
@property (weak, nonatomic) IBOutlet SKCategoryScrollView *categoryScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryScrollViewHeight;

@property (weak, nonatomic) IBOutlet SKMainTableView *leftTableVIew;
@property (weak, nonatomic) IBOutlet SKMainTableView *tableView;
@property (weak, nonatomic) IBOutlet SKMainTableView *rightTableView;

@property (weak, nonatomic) IBOutlet UILabel *leftCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *centerCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightCategoryLabel;

@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loadingViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;

@property(nonatomic, strong) UIRefreshControl *refreshControl;

//@property (weak, nonatomic) IBOutlet UIView *preView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *preViewTopMargin;
//@property (weak, nonatomic) IBOutlet UIWebView *preWebView;

@end
